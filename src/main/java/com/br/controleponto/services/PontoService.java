package com.br.controleponto.services;

import com.br.controleponto.enums.TipoBatidaEnum;
import com.br.controleponto.models.DTOs.PontoRespostaDTO;
import com.br.controleponto.models.Ponto;
import com.br.controleponto.models.Usuario;
import com.br.controleponto.repositories.PontoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Duration;
import java.time.LocalDateTime;


@Service
public class PontoService {
    @Autowired
    PontoRepository pontoRepository;
    @Autowired
    UsuarioService usuarioService;

    public Ponto baterPonto(Ponto ponto) {
        Usuario usuario = usuarioService.buscarUsuario(ponto.getUsuario().getId());
        ponto.setUsuario(usuario);
        return pontoRepository.save(ponto);
    }


    public PontoRespostaDTO pesquisaPonto(int idUsuario) {
        LocalDateTime entrada = null;
        LocalDateTime saida = null;
        Long quantidadeHoras = (long) 0;
        PontoRespostaDTO pontoRespostaDTO = new PontoRespostaDTO();


        Usuario usuario = usuarioService.buscarUsuario(idUsuario);
        Iterable<Ponto> pontosUsuario = pontoRepository.findAllByUsuario(usuario);

        for (Ponto ponto : pontosUsuario) {
            if (ponto.getTipoBatido() == TipoBatidaEnum.ENTRADA) {
                entrada = ponto.getHorarioBatida();
            } else {
                saida = ponto.getHorarioBatida();
            }

            if (entrada != null && saida != null) {
                {
                    Duration duracao = Duration.between(entrada, saida);
                    quantidadeHoras += duracao.toMinutes();
                    entrada = null;
                    saida = null;
                }
            }
        }

        pontoRespostaDTO.setPontosUsuario(pontosUsuario);
        pontoRespostaDTO.setTotalHorasTralhadas(new BigDecimal((double) quantidadeHoras / 60)
                .setScale(2, RoundingMode.HALF_UP));
        return pontoRespostaDTO;
    }


}
