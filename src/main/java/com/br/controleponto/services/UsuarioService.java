package com.br.controleponto.services;

import com.br.controleponto.models.DTOs.UsuarioDTO;
import com.br.controleponto.models.Usuario;
import com.br.controleponto.repositories.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UsuarioService {

    @Autowired
    UsuarioRepository usuarioRepository;

    public Usuario criarUsuario(Usuario usuario) {
        return usuarioRepository.save(usuario);
    }

    public Iterable<Usuario> listarUsuarios() {
        return usuarioRepository.findAll();
    }

    public Usuario buscarUsuario(int id) {
        Optional<Usuario> optionalUsuario = usuarioRepository.findById(id);
        if (optionalUsuario.isPresent()) {
            return optionalUsuario.get();
        }
        throw new RuntimeException("Usuario não encontrado");

    }

    public Usuario editarUsuario(int id, UsuarioDTO usuarioDTO) {
        Usuario usuario = buscarUsuario(id);
        usuario.setCPF(usuarioDTO.getCPF());
        usuario.setEmail(usuarioDTO.getEmail());
        usuario.setNomeUsuario(usuarioDTO.getNomeUsuario());
        return criarUsuario(usuario);
    }
}
