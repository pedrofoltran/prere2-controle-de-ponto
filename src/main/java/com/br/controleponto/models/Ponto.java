package com.br.controleponto.models;

import com.br.controleponto.enums.TipoBatidaEnum;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
public class Ponto {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idPonto;
    LocalDateTime horarioBatida;
    TipoBatidaEnum tipoBatido;
    @ManyToOne
    Usuario usuario;

    public Ponto() {
    }

    public int getIdPonto() {
        return idPonto;
    }

    public void setIdPonto(int idPonto) {
        this.idPonto = idPonto;
    }

    public LocalDateTime getHorarioBatida() {
        return horarioBatida;
    }

    public void setHorarioBatida(LocalDateTime horarioBatida) {
        this.horarioBatida = horarioBatida;
    }

    public TipoBatidaEnum getTipoBatido() {
        return tipoBatido;
    }

    public void setTipoBatido(TipoBatidaEnum tipoBatido) {
        this.tipoBatido = tipoBatido;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
}
