package com.br.controleponto.models.DTOs;

import com.br.controleponto.models.Ponto;

import java.math.BigDecimal;

public class PontoRespostaDTO {
    Iterable<Ponto> pontosUsuario;
    BigDecimal totalHorasTralhadas;

    public PontoRespostaDTO() {
    }

    public Iterable<Ponto> getPontosUsuario() {
        return pontosUsuario;
    }

    public void setPontosUsuario(Iterable<Ponto> pontosUsuario) {
        this.pontosUsuario = pontosUsuario;
    }

    public BigDecimal getTotalHorasTralhadas() {
        return totalHorasTralhadas;
    }

    public void setTotalHorasTralhadas(BigDecimal totalHorasTralhadas) {
        this.totalHorasTralhadas = totalHorasTralhadas;
    }
}
