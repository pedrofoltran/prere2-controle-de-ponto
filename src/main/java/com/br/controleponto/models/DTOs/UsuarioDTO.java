package com.br.controleponto.models.DTOs;

import org.hibernate.validator.constraints.br.CPF;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class UsuarioDTO {
    @NotEmpty(message = "Necessario preencher nome")
    @NotNull(message = "Necessario preencher nome")
    private String nomeUsuario;
    @CPF(message = "CPF invalido")
    @NotEmpty
    @NotNull
    private String CPF;
    @NotEmpty
    @NotNull
    @Email(message = "E-Mail invalido")
    private String email;

    public UsuarioDTO() {
    }

    public String getNomeUsuario() {
        return nomeUsuario;
    }

    public void setNomeUsuario(String nomeUsuario) {
        this.nomeUsuario = nomeUsuario;
    }

    public String getCPF() {
        return CPF;
    }

    public void setCPF(String CPF) {
        this.CPF = CPF;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
