package com.br.controleponto.models;


import org.hibernate.validator.constraints.br.CPF;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Entity
public class Usuario {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @NotEmpty(message = "Necessario preencher nome")
    @NotNull(message = "Necessario preencher nome")
    private String nomeUsuario;
    @NotEmpty(message = "Necessario preencher CPF")
    @NotNull(message = "Necessario preencher CPF")
    @CPF(message = "CPF invalido")
    private String CPF;
    @NotEmpty(message = "Necessario preencher EMail")
    @NotNull(message = "Necessario preencher EMail")
    @Email(message = "E-Mail invalido")
    private String email;
    @NotNull(message = "Necessario preencher Data Cadastro")
    private LocalDate dataCadastro;

    public Usuario() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNomeUsuario() {
        return nomeUsuario;
    }

    public void setNomeUsuario(String nomeUsuario) {
        this.nomeUsuario = nomeUsuario;
    }

    public String getCPF() {
        return CPF;
    }

    public void setCPF(String CPF) {
        this.CPF = CPF;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDate getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(LocalDate dataCadastro) {
        this.dataCadastro = dataCadastro;
    }
}
