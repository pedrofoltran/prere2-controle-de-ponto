package com.br.controleponto.repositories;


import com.br.controleponto.models.Ponto;
import com.br.controleponto.models.Usuario;
import org.springframework.data.repository.CrudRepository;

public interface PontoRepository extends CrudRepository<Ponto, Integer> {
    Iterable<Ponto> findAllByUsuario(Usuario usuario);
}
