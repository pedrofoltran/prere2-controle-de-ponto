package com.br.controleponto.controllers;

import com.br.controleponto.models.DTOs.PontoRespostaDTO;
import com.br.controleponto.models.Ponto;
import com.br.controleponto.services.PontoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/ponto")
public class PontoController {
    @Autowired
    PontoService pontoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Ponto baterPonto(@RequestBody @Valid Ponto ponto) {
        try {
            return pontoService.baterPonto(ponto);
        } catch (RuntimeException re) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, re.getMessage());
        }
    }

    @GetMapping("/{id}")
    public PontoRespostaDTO pesquisarApontamentos(@PathVariable(name = "id") int id) {
        try {
            return pontoService.pesquisaPonto(id);
        } catch (RuntimeException re) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, re.getMessage());
        }
    }

}
