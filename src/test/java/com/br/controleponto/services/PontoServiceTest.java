package com.br.controleponto.services;

import com.br.controleponto.enums.TipoBatidaEnum;
import com.br.controleponto.models.DTOs.PontoRespostaDTO;
import com.br.controleponto.models.Ponto;
import com.br.controleponto.models.Usuario;
import com.br.controleponto.repositories.PontoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;

@SpringBootTest
public class PontoServiceTest {

    @MockBean
    private PontoRepository pontoRepository;
    @MockBean
    private UsuarioService usuarioService;

    @Autowired
    private PontoService pontoService;

    Usuario usuarioTeste, usuarioEntrada;
    Ponto pontoTesteEntrada, pontoTesteSaida;
    Iterable<Ponto> pontos;

    @BeforeEach
    public void Setup() {

        usuarioTeste = new Usuario();
        usuarioEntrada = new Usuario();
        pontoTesteEntrada = new Ponto();
        pontoTesteSaida = new Ponto();

        usuarioTeste.setNomeUsuario("Oliver");
        usuarioTeste.setEmail("oliveira@email.com");
        usuarioTeste.setCPF("618.200.390-13");
        usuarioTeste.setDataCadastro(LocalDate.now());
        usuarioTeste.setId(2);

        usuarioEntrada.setId(2);

        pontoTesteEntrada.setIdPonto(1);
        pontoTesteEntrada.setUsuario(usuarioTeste);
        pontoTesteEntrada.setTipoBatido(TipoBatidaEnum.ENTRADA);
        pontoTesteEntrada.setHorarioBatida(LocalDateTime.parse("2020-07-02T02:00:00"));

        pontoTesteSaida.setIdPonto(1);
        pontoTesteSaida.setUsuario(usuarioTeste);
        pontoTesteSaida.setTipoBatido(TipoBatidaEnum.SAIDA);
        pontoTesteSaida.setHorarioBatida(LocalDateTime.parse("2020-07-02T10:30:00"));

        Mockito.when(usuarioService.buscarUsuario(Mockito.anyInt())).thenReturn(usuarioTeste);
    }

    @Test
    public void testarBaterPonto() {
        Mockito.when(pontoRepository.save(Mockito.any(Ponto.class))).thenReturn(pontoTesteEntrada);

        Ponto pontoResp = pontoService.baterPonto(pontoTesteEntrada);
        Assertions.assertEquals(pontoResp.getUsuario(), usuarioTeste);
    }

    @Test
    public void testarBaterPontoUsuarioNaoExiste() {
        Mockito.when(usuarioService.buscarUsuario(Mockito.anyInt()))
                .thenThrow(new RuntimeException("Usuario não encontrado") {
                });

        Assertions.assertThrows(RuntimeException.class, () -> {
            pontoService.baterPonto(pontoTesteEntrada);
        });
    }

    @Test
    public void testarPesquisaPonto() {
        pontos = Arrays.asList(pontoTesteEntrada, pontoTesteSaida);
        Mockito.when(pontoRepository.findAllByUsuario(Mockito.any(Usuario.class))).thenReturn(pontos);

        PontoRespostaDTO pontoRespostaDTO = pontoService.pesquisaPonto(2);
        Assertions.assertEquals(pontoRespostaDTO.getTotalHorasTralhadas(), BigDecimal.valueOf(8.50)
                .setScale(2, RoundingMode.HALF_UP));
    }

    @Test
    public void testarPesquisaPontoUsuarioNaoEcontrado() {
        pontos = Arrays.asList(pontoTesteEntrada, pontoTesteSaida);
        Mockito.when(usuarioService.buscarUsuario(Mockito.anyInt()))
                .thenThrow(new RuntimeException("Usuario não encontraddo") {
                });

        Assertions.assertThrows(RuntimeException.class, () -> {
            pontoService.pesquisaPonto(2);
        });

    }

}
