package com.br.controleponto.services;

import com.br.controleponto.models.DTOs.UsuarioDTO;
import com.br.controleponto.models.Usuario;
import com.br.controleponto.repositories.UsuarioRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Optional;

@SpringBootTest
public class UsuarioServiceTest {

    @MockBean
    private UsuarioRepository usuarioRepository;

    @Autowired
    private UsuarioService usuarioService;

    Usuario usuario, usuarioTeste;

    @BeforeEach
    public void Setup() {
        LocalDate localDate = LocalDate.now();

        usuario = new Usuario();
        usuario.setNomeUsuario("Oliver");
        usuario.setEmail("oliveira@email.com");
        usuario.setCPF("618.200.390-13");
        usuario.setDataCadastro(localDate);

        usuarioTeste = new Usuario();
        usuarioTeste.setNomeUsuario("Oliver");
        usuarioTeste.setEmail("oliveira@email.com");
        usuarioTeste.setCPF("618.200.390-13");
        usuarioTeste.setDataCadastro(localDate);
        usuarioTeste.setId(2);
    }

    @Test
    public void testarListarUsuario() {
        Iterable<Usuario> usuarios = Arrays.asList(usuario);
        Mockito.when(usuarioRepository.findAll()).thenReturn(usuarios);

        Iterable<Usuario> usuarioIterable = usuarioService.listarUsuarios();

        Assertions.assertEquals(usuarios, usuarioIterable);
    }

    @Test
    public void testarCriarUsuario() {
        Mockito.when(usuarioRepository.save(usuario)).thenReturn(usuarioTeste);

        Usuario usuarioResp = usuarioService.criarUsuario(usuario);
        Assertions.assertEquals(usuarioResp.getDataCadastro(), usuarioTeste.getDataCadastro());
    }

    @Test
    public void testarBuscarUsuario() {
        Optional<Usuario> usuarioOptional = Optional.of(usuario);
        Mockito.when(usuarioRepository.findById(Mockito.anyInt())).thenReturn(usuarioOptional);
        Usuario usuarioResp = usuarioService.buscarUsuario(1);
        Assertions.assertEquals(usuario, usuarioResp);
    }

    @Test
    public void testarBuscarUsuarioInexistente() {
        Optional<Usuario> usuarioOptional = Optional.empty();
        Mockito.when(usuarioRepository.findById(Mockito.anyInt())).thenReturn(usuarioOptional);
        Assertions.assertThrows(RuntimeException.class, () -> {
            usuarioService.buscarUsuario(222);
        });
    }

    @Test
    public void testarEditarUsuario() {
        UsuarioDTO usuarioDTO = new UsuarioDTO();
        usuario.setId(2);
        Optional<Usuario> usuarioOptional = Optional.of(usuario);
        Mockito.when(usuarioRepository.findById(Mockito.anyInt())).thenReturn(usuarioOptional);

        usuarioTeste.setId(2);
        usuarioTeste.setCPF("472.063.320-05");
        usuarioTeste.setEmail("luiza@email.com");
        usuarioTeste.setNomeUsuario("Luiza");
        Mockito.when(usuarioRepository.save(Mockito.any(Usuario.class))).thenReturn(usuarioTeste);

        usuarioDTO.setCPF("472.063.320-05");
        usuarioDTO.setEmail("luiza@email.com");
        usuarioDTO.setNomeUsuario("Luiza");
        Usuario usuarioResp = usuarioService.editarUsuario(2, usuarioDTO);

        Assertions.assertEquals(2, usuarioResp.getId());
    }

    @Test
    public void testarEditarUsuarioNaoExiste() {
        UsuarioDTO usuarioDTO = new UsuarioDTO();
        usuarioDTO.setCPF("472.063.320-05");
        usuarioDTO.setEmail("luiza@email.com");
        usuarioDTO.setNomeUsuario("Luiza");

        Optional<Usuario> usuarioOptional = Optional.empty();
        Mockito.when(usuarioRepository.findById(Mockito.anyInt())).thenReturn(usuarioOptional);

        Assertions.assertThrows(RuntimeException.class, () -> {
            usuarioService.editarUsuario(2, usuarioDTO);
        });
    }
}
