package com.br.controleponto.controllers;


import com.br.controleponto.models.DTOs.UsuarioDTO;
import com.br.controleponto.models.Usuario;
import com.br.controleponto.services.UsuarioService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDate;
import java.util.Arrays;

import static org.hamcrest.Matchers.hasSize;

@WebMvcTest(UsuarioController.class)
public class UsuarioControllerTest {

    @MockBean
    private UsuarioService usuarioService;

    @Autowired
    private MockMvc mockMvc;

    Usuario usuario, usuarioTeste;

    @BeforeEach
    public void Setup() {
        LocalDate localDate = LocalDate.now();

        usuario = new Usuario();
        usuario.setNomeUsuario("Oliver");
        usuario.setEmail("oliveira@email.com");
        usuario.setCPF("472.063.320-05");
        usuario.setDataCadastro(localDate);

        usuarioTeste = new Usuario();
        usuarioTeste.setNomeUsuario("Luiza");
        usuarioTeste.setEmail("luiza@email.com");
        usuarioTeste.setCPF("482.512.570-00");
        usuarioTeste.setDataCadastro(localDate);
        usuarioTeste.setId(2);
    }


    @Test
    public void testarCriarUsuario() throws Exception {
        usuario.setDataCadastro(null);
        usuario.setId(1);

        ObjectMapper mapper = new ObjectMapper();
        String usuarioJson = mapper.writeValueAsString(usuario);
        usuarioJson = usuarioJson.replace("\"dataCadastro\":null,", "\"dataCadastro\":\"2020-07-03\",");

        usuario.setDataCadastro(LocalDate.of(2020, 07, 03));

        Mockito.when(usuarioService.criarUsuario(Mockito.any(Usuario.class))).thenReturn(usuario);
        mockMvc.perform(MockMvcRequestBuilders.post("/usuario")
                .contentType(MediaType.APPLICATION_JSON)
                .content(usuarioJson))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)));
    }


    @Test
    public void testarCriarUsuarioErroEntrada() throws Exception {
        usuario.setDataCadastro(null);
        usuario.setCPF("1111111111");

        ObjectMapper mapper = new ObjectMapper();
        String usuarioJson = mapper.writeValueAsString(usuario);
        usuarioJson = usuarioJson.replace("\"dataCadastro\":null,", "\"dataCadastro\":\"2020-07-03\",");

        usuario.setDataCadastro(LocalDate.of(2020, 07, 03));

        Mockito.when(usuarioService.criarUsuario(Mockito.any(Usuario.class))).
                thenThrow(new RuntimeException("CPF incorreto") {
                });
        mockMvc.perform(MockMvcRequestBuilders.post("/usuario")
                .contentType(MediaType.APPLICATION_JSON)
                .content(usuarioJson))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void testarEditarUsuario() throws Exception {
        UsuarioDTO usuarioDTO = new UsuarioDTO();
        usuarioDTO.setCPF("482.512.570-00");
        usuarioDTO.setEmail("luiza@email.com");
        usuarioDTO.setNomeUsuario("Luiza");

        ObjectMapper mapper = new ObjectMapper();
        String usuarioJson = mapper.writeValueAsString(usuario);

        Mockito.when(usuarioService.editarUsuario(Mockito.anyInt(), Mockito.any(UsuarioDTO.class)))
                .thenReturn(usuarioTeste);

        mockMvc.perform(MockMvcRequestBuilders.put("/usuario/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(usuarioJson))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nomeUsuario",
                        CoreMatchers.equalTo("Luiza")));
    }

    @Test
    public void testarEditarUsuarioInexistente() throws Exception {
        UsuarioDTO usuarioDTO = new UsuarioDTO();
        usuarioDTO.setCPF("482.512.570-00");
        usuarioDTO.setEmail("luiza@email.com");
        usuarioDTO.setNomeUsuario("Luiza");

        ObjectMapper mapper = new ObjectMapper();
        String usuarioJson = mapper.writeValueAsString(usuario);

        Mockito.when(usuarioService.editarUsuario(Mockito.anyInt(), Mockito.any(UsuarioDTO.class)))
                .thenThrow(new RuntimeException("Usuario não encontrado") {
                });

        mockMvc.perform(MockMvcRequestBuilders.put("/usuario/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(usuarioJson))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void testarListarUsuarios() throws Exception {
        Iterable<Usuario> usuarios = Arrays.asList(usuario, usuarioTeste);
        Mockito.when(usuarioService.listarUsuarios()).thenReturn(usuarios);

        mockMvc.perform(MockMvcRequestBuilders.get("/usuario/")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.*", hasSize(2)));
    }

    @Test
    public void testarBuscarUsuario() throws Exception {
        Mockito.when(usuarioService.buscarUsuario(Mockito.anyInt())).thenReturn(usuarioTeste);

        mockMvc.perform(MockMvcRequestBuilders.get("/usuario/2")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists());
    }

    @Test
    public void testarBuscarUsuarioInexistente() throws Exception {
        Mockito.when(usuarioService.buscarUsuario(Mockito.anyInt()))
                .thenThrow(new RuntimeException("Usuario não encontrado") {
                });

        mockMvc.perform(MockMvcRequestBuilders.get("/usuario/2")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }
}
