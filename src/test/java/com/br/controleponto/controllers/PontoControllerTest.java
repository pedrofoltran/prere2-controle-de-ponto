package com.br.controleponto.controllers;

import com.br.controleponto.enums.TipoBatidaEnum;
import com.br.controleponto.models.DTOs.PontoRespostaDTO;
import com.br.controleponto.models.Ponto;
import com.br.controleponto.models.Usuario;
import com.br.controleponto.services.PontoService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;

@WebMvcTest(PontoController.class)
public class PontoControllerTest {
    @MockBean
    private PontoService pontoService;

    @Autowired
    private MockMvc mockMvc;

    Usuario usuarioEntrada, usuarioTeste;
    Ponto pontoTesteEntrada, pontoTesteSaida;

    @BeforeEach
    public void Setup() {

        usuarioTeste = new Usuario();
        usuarioEntrada = new Usuario();
        pontoTesteEntrada = new Ponto();
        pontoTesteSaida = new Ponto();

        usuarioTeste.setNomeUsuario("Oliver");
        usuarioTeste.setEmail("oliveira@email.com");
        usuarioTeste.setCPF("618.200.390-13");
        usuarioTeste.setDataCadastro(LocalDate.now());
        usuarioTeste.setId(2);

        usuarioEntrada.setId(2);

        pontoTesteEntrada.setIdPonto(1);
        pontoTesteEntrada.setUsuario(usuarioEntrada);
        pontoTesteEntrada.setTipoBatido(TipoBatidaEnum.ENTRADA);
        pontoTesteEntrada.setHorarioBatida(LocalDateTime.parse("2020-07-02T02:00:00"));

        pontoTesteSaida.setIdPonto(1);
        pontoTesteSaida.setUsuario(usuarioEntrada);
        pontoTesteSaida.setTipoBatido(TipoBatidaEnum.SAIDA);
        pontoTesteSaida.setHorarioBatida(LocalDateTime.parse("2020-07-02T10:30:00"));

    }

    @Test
    public void testarBaterPonto() throws Exception {
        pontoTesteEntrada.setHorarioBatida(null);

        ObjectMapper mapper = new ObjectMapper();
        String pontoJson = mapper.writeValueAsString(pontoTesteEntrada);
        pontoJson = pontoJson
                .replace("\"horarioBatida\":null,", "\"horarioBatida\":\"2020-07-02T02:00:00\",");

        pontoTesteEntrada.setHorarioBatida(LocalDateTime.parse("2020-07-02T02:00:00"));

        Mockito.when(pontoService.baterPonto(Mockito.any(Ponto.class))).thenReturn(pontoTesteEntrada);
        mockMvc.perform(MockMvcRequestBuilders.post("/ponto")
                .contentType(MediaType.APPLICATION_JSON)
                .content(pontoJson))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Test
    public void testarBaterPontoUsuarioInexiste() throws Exception {
        pontoTesteEntrada.setHorarioBatida(null);

        ObjectMapper mapper = new ObjectMapper();
        String pontoJson = mapper.writeValueAsString(pontoTesteEntrada);
        pontoJson = pontoJson
                .replace("\"horarioBatida\":null,", "\"horarioBatida\":\"2020-07-02T02:00:00\",");

        pontoTesteEntrada.setHorarioBatida(LocalDateTime.parse("2020-07-02T02:00:00"));

        Mockito.when(pontoService.baterPonto(Mockito.any(Ponto.class)))
                .thenThrow(new RuntimeException("Usuario não encontrado") {
                });
        mockMvc.perform(MockMvcRequestBuilders.post("/ponto")
                .contentType(MediaType.APPLICATION_JSON)
                .content(pontoJson))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void testarHorasTrabalhadas() throws Exception {
        Iterable<Ponto> pontos = Arrays.asList(pontoTesteEntrada, pontoTesteSaida);

        PontoRespostaDTO pontoRespostaDTO = new PontoRespostaDTO();
        pontoRespostaDTO.setTotalHorasTralhadas(BigDecimal.valueOf(8.50)
                .setScale(2, RoundingMode.HALF_UP));
        pontoRespostaDTO.setPontosUsuario(pontos);

        Mockito.when(pontoService.pesquisaPonto(Mockito.anyInt())).thenReturn(pontoRespostaDTO);

        mockMvc.perform(MockMvcRequestBuilders.get("/ponto/2"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers
                        .jsonPath("$.totalHorasTralhadas", CoreMatchers.equalTo(8.50)));

    }

    @Test
    public void testarHorasTrabalhadasUsuarioInexistente() throws Exception {
        Mockito.when(pontoService.pesquisaPonto(Mockito.anyInt())).
                thenThrow(new RuntimeException("Usuario não encontrado") {
                });
        mockMvc.perform(MockMvcRequestBuilders.get("/ponto/2"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

}
