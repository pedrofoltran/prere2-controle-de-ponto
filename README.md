End points API:<br />
<br />
<br />
################## USUARIO ##################<br />
Objetivo: Cadastrar usuário<br />
Método: POST<br />
Endereço: localhost:8080/usuario/<br />
Body:<br />
{<br />
    "nomeUsuario":"Pedro",<br />
    "cpf":"026.449.870-40,<br />
    "email":"pedro@email",<br />
    "dataCadastro":"2020-02-20"<br />
}<br />
<br />
Objetivo: Retornar lista de usuário<br />
Método: GET<br />
Endereço: localhost:8080/usuario/<br />
<br />
Objetivo: Retornar informações de usuário específico<br />
Método: GET<br />
Endereço: localhost:8080/usuario/{id}<br />
<br />
Objetivo: Alterar informações de usuário específico<br />
Método: PUT<br />
Endereço: localhost:8080/usuario/{id}<br />
Body:<br />
{<br />
    "nomeUsuario":"Alice",<br />
    "cpf":"282.091.850-63",<br />
    "email":"alice@email.com.br"<br />
}<br />
<br />
################## PONTO ##################<br />
<br />
Objetivo: Bater ponto de Entrada<br />
Método: POST<br />
Endereço: localhost:8080/ponto<br />
Body:<br />
{<br />
    "horarioBatida":"2020-08-12T10:00:26",<br />
    "tipoBatido": "ENTRADA",<br />
    "usuario":{<br />
        "id":"1"<br />
    }<br />
}<br />
<br />
Objetivo: Bater ponto de Saída<br />
Método: POST<br />
Endereço: localhost:8080/ponto<br />
Body:<br />
{<br />
    "horarioBatida":"2020-08-12T10:00:26",<br />
    "tipoBatido": "SAIDA",<br />
    "usuario":{<br />
        "id":"1"<br />
    }<br />
}<br />
<br />
Objetivo: Retornar lista de pontos por usuário especifico<br />
Método: GET<br />
Endereço: localhost:8080/ponto/{id}<br />



